package com.malyshev;

import com.malyshev.entities.IEntity;
import com.malyshev.entities.Student;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Created by jiexa on 16.02.17.
 */
public class DataBaseManager /*<T extends IEntity>*/ {

  private static final DataBaseManager dataBaseManager = new DataBaseManager();

  public static DataBaseManager getInstance() {
    return dataBaseManager == null ? new DataBaseManager() : dataBaseManager;
  }

  private Connection conn;
  
  public Connection getConnection() throws SQLException, ClassNotFoundException {
    Class.forName("org.postgresql.Driver");
    conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/StudentsDB", "postgres", "postgres");
    return conn;
  }


  public void insert(String schema, String table, Map<String, Object> map) throws SQLException {
    String query = "INSERT INTO " + schema + "." + table;
    query = query + getDynamicPartForInsert(map);
    PreparedStatement preparedStatement = conn.prepareStatement(query);

    int i = 1;
    for (String key : map.keySet()) {
      if (map.get(key) instanceof String) {
        preparedStatement.setString(i++, (String) map.get(key));
      }
      if (map.get(key) instanceof Integer) {
        preparedStatement.setInt(i++, (Integer) map.get(key));
      }
      if (map.get(key) instanceof Date) {
        preparedStatement.setDate(i++, (Date) map.get(key));
      }
      if (map.get(key) instanceof Boolean) {
        preparedStatement.setBoolean(i++, (Boolean) map.get(key));
      }
    }
    preparedStatement.executeUpdate();

  }

  private String getDynamicPartForInsert(Map<String, Object> map) {
    String str = " (";
    StringBuilder sb = new StringBuilder();
    sb.append(str);

    int k = 1;
    for (String key : map.keySet()) {
      sb.append(key);
      if (map.size() != k++) {
        sb.append(", ");
      } else {
        sb.append(") ");
      }
    }
    sb.append("Values (");

    for (int i = 0; i < map.size(); i++) {
      if (map.size() != i + 1) {
        sb.append("?, ");
      } else {
        sb.append("?)");
      }
    }
    return sb.toString();
  }

  public List<Student> selectAllStudents(String schema, String table)
       throws SQLException, ClassNotFoundException {
    String query = "Select * from " + schema + "." + table;
    PreparedStatement preparedStatement = getConnection().prepareStatement(query);
    ResultSet rs = preparedStatement.executeQuery();
    
    List<Student> studentList = new ArrayList<>(  );
  
    while ( rs.next() ){
    Student student = new Student();
    student.setId( rs.getInt( "id" ) );
    student.setName(rs.getString( "name" ));
    student.setBirthdate( rs.getDate( "birthdate" ) );
    student.setMale( rs.getBoolean( "ismale" ) );
    student.setGroupId( rs.getInt( "group_id" ) );
    
      studentList.add( student );
    }
    return studentList;

  }

  public void delete(String schema, String table, Integer id) throws SQLException {
    String query = "DELETE FROM " + schema + "." + table + " WHERE id = ?";
    PreparedStatement preparedStatement = conn.prepareStatement(query);
    preparedStatement.setInt(1, id);
    preparedStatement.executeUpdate();
  }



//  private ResultSet getResultSet(PreparedStatement preparedStatement) {
//    ResultSet resultSet = preparedStatement
//
//
//    return resultSet;
//  }
}
