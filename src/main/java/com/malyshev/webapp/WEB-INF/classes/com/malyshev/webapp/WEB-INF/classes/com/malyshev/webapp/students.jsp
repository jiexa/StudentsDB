<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Spisok</title>
</head>
<body>
<h1>You can see list of Students here</h1>
<c:forEach items="${students}" var="item">
    <c:out value="${item.name}"/><br />
</c:forEach>
</body>
</html>
