package com.malyshev;

import com.malyshev.entities.Group;
import com.malyshev.entities.Student;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jiexa on 16.02.17.
 */
public class GroupTableManager extends DataBaseManager{

  /*
  private Connection conn;

  public GroupTableManager() throws SQLException {
    this.conn = DataBaseManager.getConnection();
  }

  @Override
  public List<Group> selectAll() throws SQLException {

    String q = "Select * from main.group";

    PreparedStatement preparedStatement = conn.prepareStatement(q);
    ResultSet resultSet = preparedStatement.executeQuery();

    List<Group> list = new ArrayList<>();
    while (resultSet.next()) {
      Group group =  new Group();
      group.setId(resultSet.getInt("id"));
      group.setName(resultSet.getString("name"));
      group.setNumber(resultSet.getInt("number"));
      list.add(group);
    }
    System.out.println("\nData are selected \n");

    return list;
  }

  @Override
  public Group selectById(Integer id) throws SQLException {

    String q = "Select * from main.group where id = ?";
    PreparedStatement preparedStatement = conn.prepareStatement(q);
    preparedStatement.setInt(1, id);
    ResultSet resultSet = preparedStatement.executeQuery();

    Group group = fillGroup(resultSet);
    return group;
  }

  @Override
  public Group selectByName(String name) throws SQLException {

    String q = "Select * from main.group where name = ?";
    PreparedStatement preparedStatement = conn.prepareStatement(q);
    preparedStatement.setString(1, name);
    ResultSet resultSet = preparedStatement.executeQuery();

    Group group = fillGroup(resultSet);

    return group;
  }

  private Group fillGroup(ResultSet resultSet) throws SQLException {
    Group group = new Group();
    while (resultSet.next()) {
      group.setId(resultSet.getInt("id"));
      group.setName(resultSet.getString("name"));
      group.setNumber(resultSet.getInt("number"));
    }
    return group;
  }

  @Override
  public void insert(Group group) throws SQLException {
    String query = "Insert into main.group (name, number)" + "Values (?, ?)";
    PreparedStatement preparedStatement = conn.prepareStatement(query);
    preparedStatement.setString(1, group.getName());
    preparedStatement.setInt(2, group.getNumber());
    preparedStatement.executeUpdate();
    System.out.println("Data were inserted");
  }

  @Override
  public void update(Group group) throws SQLException {

  }

  @Override
  public void delete(Group group) throws SQLException {

  }
*/
}
