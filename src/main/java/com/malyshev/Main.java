package com.malyshev;

import com.malyshev.entities.Group;
import com.malyshev.entities.Student;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

public class Main {


    public static void main(String[] args) throws SQLException {

        Student student = new Student();
        student.setName("John");
        student.setBirthdate(new Date(1999, 02, 3));
        student.setMale(true);
        student.setGroupId(1);

//        Map<String, Object> map = new HashMap<>();
//        map.put("name", "John");
//        map.put("ismale", true);
//        map.put("birthdate", new Date(1999, 02, 3));
//        map.put("group_id", 1);

        Map<String, Object> map = new HashMap<>();
        map.put("name", "BigData");
        map.put("number", 201);

        DataBaseManager dbm = DataBaseManager.getInstance();
//        dbm.getConnection();

//        dbm.insert("main", "group", map);
        
//        dbm.selectAll("main", "student");


        Group group = new Group();
        group.setName("MSIT-SE");
        group.setNumber(2017);
    }


    public static void insertDB(String url, String login, String password) {

        try (Connection conn = DriverManager.getConnection(url, login, password)) {
            String sqlQ = "Insert into main.students (id, name, birthdate, sex, group_id)" + "Values (?, ?, ?, ?, ?)";
            PreparedStatement preparedStatement = conn.prepareStatement(sqlQ);
            preparedStatement.setInt(1, 5);
            preparedStatement.setString(2, "Fred");
            preparedStatement.setDate(3, new java.sql.Date(1988, 8, 1));
            preparedStatement.setString(4, "М");
            preparedStatement.setInt(5, 1);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void selectDB(String url, String login, String password) throws ClassNotFoundException {
        Class.forName("org.postgresql.Driver");

        try (Connection conn = DriverManager.getConnection(url, login, password)) {
            Statement query = conn.createStatement();
            ResultSet resultSet = query.executeQuery("Select * from main.students");
            while (resultSet.next()) {
                System.out.println(resultSet.getString("name"));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


}
