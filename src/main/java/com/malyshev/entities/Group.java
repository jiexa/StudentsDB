package com.malyshev.entities;

/**
 * Created by jiexa on 16.02.17.
 */
public class Group implements IEntity {

  private Integer id;
  private String name;
  private Integer number;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getNumber() {
    return number;
  }

  public void setNumber(Integer number) {
    this.number = number;
  }
}
