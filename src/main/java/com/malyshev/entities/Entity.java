package com.malyshev.entities;

/**
 * Created by jiexa on 19.02.17.
 */
public class Entity<T> {
  private Integer id;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }
}
