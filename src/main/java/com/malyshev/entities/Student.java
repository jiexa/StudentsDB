package com.malyshev.entities;

import java.sql.Date;

/**
 * Created by jiexa on 16.02.17.
 */
public class Student implements IEntity {

  private Integer id;
  private String name;
  private Date birthdate;
  private boolean isMale;
  private Integer groupId;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Date getBirthdate() {
    return birthdate;
  }

  public void setBirthdate(Date birthdate) {
    this.birthdate = birthdate;
  }

  public boolean getMale() {
    return isMale;
  }

  public void setMale(boolean male) {
    this.isMale = male;
  }

  public Integer getGroupId() {
    return groupId;
  }

  public void setGroupId(Integer groupId) {
    this.groupId = groupId;
  }
}
