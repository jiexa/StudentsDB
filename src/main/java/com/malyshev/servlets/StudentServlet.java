package com.malyshev.servlets;

import com.malyshev.DataBaseManager;
import com.malyshev.entities.Student;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by A.Malyshev on 22.02.17.
 */
public class StudentServlet extends HttpServlet {
      
      @Override
      protected void doGet( HttpServletRequest req, HttpServletResponse resp )
           throws ServletException, IOException {
      
            DataBaseManager dbm = DataBaseManager.getInstance();
            try {
                  dbm.getConnection();
            } catch ( SQLException e ) {
                  e.printStackTrace( );
            } catch ( ClassNotFoundException e ) {
                  e.printStackTrace( );
            }
      
            List< Student > students = null;
            try {
                  students = dbm.selectAllStudents("main", "student");
            } catch ( SQLException e ) {
                  e.printStackTrace( );
            } catch ( ClassNotFoundException e ) {
                  e.printStackTrace( );
            }
            req.setAttribute( "students", students );
            req.getRequestDispatcher( "/students.jsp" ).forward( req, resp );
      }
      
      @Override
      protected void doPost( HttpServletRequest req, HttpServletResponse resp )
           throws ServletException, IOException {
            
      }
}
