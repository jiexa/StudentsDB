package com.malyshev;

import com.malyshev.entities.Student;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jiexa on 16.02.17.
 */

public class StudentTableManager extends DataBaseManager{
/*
  private Connection conn;

  public StudentTableManager() throws SQLException {
    this.conn = DataBaseManager.getConnection();
  }

  @Override
  public List<Student> selectAll() throws SQLException {

    String q = "Select * from main.student";

    PreparedStatement preparedStatement = conn.prepareStatement(q);
    ResultSet resultSet = preparedStatement.executeQuery();

    List<Student> list = new ArrayList<>();
    while (resultSet.next()) {
      Student student =  new Student();
      student.setId(resultSet.getInt("id"));
      student.setName(resultSet.getString("name"));
      student.setBirthdate(resultSet.getDate("birthdate"));
      student.setMale(resultSet.getBoolean("ismale"));
      student.setGroupId(resultSet.getInt("group_id"));
      list.add(student);
    }
    System.out.println("\nData are selected \n");

    return list;
  }

  @Override
  public Student selectById(Integer id) throws SQLException {
    String q = "Select * from main.student s where s.id = ?";
    PreparedStatement preparedStatement = conn.prepareStatement(q);
    preparedStatement.setInt(1, id);
    ResultSet resultSet = preparedStatement.executeQuery();

    Student student = fillStudent(resultSet);

    System.out.println("\nData are selected \n");
    return student;
  }
  @Override
  public Student selectByName(String name) throws SQLException {
    String q = "Select * from main.student s where s.name = ?";
    PreparedStatement preparedStatement = conn.prepareStatement(q);
    preparedStatement.setString(1, name);
    ResultSet resultSet = preparedStatement.executeQuery();

    Student student = fillStudent(resultSet);

    System.out.println("\nData are selected \n");
    return student;
  }

  private Student fillStudent(ResultSet resultSet) throws SQLException {
    Student student = new Student();
    while (resultSet.next()) {
      student.setId(resultSet.getInt("id"));
      student.setName(resultSet.getString("name"));
      student.setBirthdate(resultSet.getDate("birthdate"));
      student.setMale(resultSet.getBoolean("ismale"));
      student.setGroupId(resultSet.getInt("group_id"));
    }
    return student;
  }

  public void insert(Student student) throws SQLException {

    String query = "Insert into main.student (name, birthdate, ismale, group_id)" + "Values (?, ?, ?, ?)";
    PreparedStatement preparedStatement = conn.prepareStatement(query);
    preparedStatement.setString(1, student.getName());
    preparedStatement.setDate(2, student.getBirthdate());
    preparedStatement.setBoolean(3, student.getMale());
    preparedStatement.setInt(4, student.getGroupId());
    preparedStatement.executeUpdate();
    System.out.println("Data were inserted");
  }

  @Override
  public void update(Student obj) throws SQLException {

  }

  @Override
  public void delete(Student obj) throws SQLException {

  }
*/

}
